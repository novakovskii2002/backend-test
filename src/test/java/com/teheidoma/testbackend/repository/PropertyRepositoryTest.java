package com.teheidoma.testbackend.repository;

import com.teheidoma.testbackend.GeoUtils;
import com.teheidoma.testbackend.TestUtils;
import com.teheidoma.testbackend.model.Property;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.iterableWithSize;

@DataJpaTest(showSql = true)
@ExtendWith(SpringExtension.class)
@Sql("/schema-h2.sql")
public class PropertyRepositoryTest {

    @Autowired
    private PropertyRepository propertyRepository;

    @Test
    void findInArea() {
        final Property property = TestUtils.createTestProperty();

        propertyRepository.save(property);

        List<Property> properties = propertyRepository.findInArea(GeoUtils.createPolygon(
                GeoUtils.createPoint(1.5, 0.5),
                GeoUtils.createPoint(0.5, 1.5)
        ));

        MatcherAssert.assertThat(properties, iterableWithSize(1));
        MatcherAssert.assertThat(properties.get(0), equalTo(property));
    }
}
