package com.teheidoma.testbackend.service;

import com.teheidoma.testbackend.GeoUtils;
import com.teheidoma.testbackend.TestUtils;
import com.teheidoma.testbackend.model.Property;
import com.teheidoma.testbackend.repository.PropertyRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.locationtech.jts.geom.Polygon;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class PropertyServiceTest {
    @Mock
    private PropertyRepository repository;

    @InjectMocks
    private PropertyService propertyService;

    @Test
    void create() {
        final Property property = TestUtils.createTestProperty();

        propertyService.create(property.getId(),
                               property.getRoomsCount(),
                               property.getSquare(),
                               property.getAddress(),
                               property.getDescription(),
                               property.getMedia(),
                               property.getPoint());

        verify(repository, Mockito.atLeastOnce()).save(Mockito.any(Property.class));
    }

    @Test
    void getAll() {
        when(repository.findAll()).thenReturn(Arrays.asList(new Property(), new Property()));

        final List<Property> all = propertyService.getAll();

        assertThat(all.size(), is(2));
    }

    @Test
    void getAllInArea() {
        final Property property = TestUtils.createTestProperty(1.5, 1.5);

        when(repository.findInArea(Mockito.any(Polygon.class))).thenReturn(Collections.singletonList(property));

        final List<Property> list = propertyService.getAllInArea(GeoUtils.createPolygon(
                GeoUtils.createPoint(2, 0),
                GeoUtils.createPoint(0, 2)
        ));

        assertThat(list.size(), is(1));
        assertThat(list.get(0), equalTo(property));

    }

    @Test
    void getById() {
        Property property = TestUtils.createTestProperty();
        long id = property.getId();

        when(repository.findById(id)).thenReturn(Optional.of(property));

        Optional<Property> optionalProperty = propertyService.getById(id);

        assertThat(optionalProperty.isPresent(), is(true));
        assertThat(optionalProperty.get(), equalTo(property));
    }
}
