package com.teheidoma.testbackend.service;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@SpringBootTest
class MediaServiceTest {

    private static final String TEST_FILE_NAME = "test.png";

    @Autowired
    private MediaService mediaService;

    @Test
    void saveImage() throws IOException {
        final InputStream input = loadTestImage();
        final String name = mediaService.saveImage(input, TEST_FILE_NAME);
        final CloseableHttpResponse response = HttpClients.createMinimal().execute(new HttpGet(name));

        assertThat(response.getStatusLine().getStatusCode(), is(200));

        mediaService.delete(name);
    }

    private InputStream loadTestImage() {
        return getClass().getClassLoader().getResourceAsStream("test.png");
    }
}
