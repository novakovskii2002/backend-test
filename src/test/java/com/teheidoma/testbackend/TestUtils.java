package com.teheidoma.testbackend;

import com.teheidoma.testbackend.model.Property;

public class TestUtils {
    public static Property createTestProperty(){
        return Property.builder()
                .id(1)
                .address("test")
                .description("test")
                .roomsCount(1)
                .square(1.0)
                .point(GeoUtils.createPoint(1, 1))
                .build();
    }

    public static Property createTestProperty(double lat, double lon){
        return Property.builder()
                .id(1)
                .address("test")
                .description("test")
                .roomsCount(1)
                .square(1.0)
                .point(GeoUtils.createPoint(lat, lon))
                .build();
    }


}
