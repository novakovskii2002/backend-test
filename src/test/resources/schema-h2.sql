CREATE
ALIAS IF NOT EXISTS H2GIS_SPATIAL FOR "org.h2gis.functions.factory.H2GISFunctions.load";
CALL H2GIS_SPATIAL();

drop table if exists property;
create table property
(
    id          long           not null
        constraint property_pkey primary key,
    address     varchar(255)     not null,
    description varchar(255)     not null,
    media       varchar(255),
    point       geometry(4326),
    rooms_count integer          not null,
    square      double not null
);

create index point_index
    on property (point);

create sequence if not exists HIBERNATE_SEQUENCE start with 1 increment by 50



