package com.teheidoma.testbackend.model;


import com.teheidoma.testbackend.config.NotZero;
import com.teheidoma.testbackend.config.converter.JsonMediaAttributeConverter;
import lombok.*;
import org.locationtech.jts.geom.Point;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "property",
        indexes = {
                @Index(name = "point_index", columnList = "point")
        })
public class Property {
    @Id
    @GeneratedValue
    private long id;

    @NotZero
    @Column(name = "rooms_count", nullable = false)
    private Integer roomsCount;

    @NotZero
    @Column(name = "square", nullable = false)
    private Double square;

    @Column(name = "description", nullable = false)
    private String description;

    @NotZero
    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "media", columnDefinition = "json")
    @Convert(converter = JsonMediaAttributeConverter.class)
    private List<String> media;

    @Column(columnDefinition = "geometry(Point,4326)")
    private Point point;
}
