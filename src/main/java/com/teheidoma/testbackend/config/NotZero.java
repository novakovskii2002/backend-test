package com.teheidoma.testbackend.config;

import org.hibernate.validator.constraints.CompositionType;
import org.hibernate.validator.constraints.ConstraintComposition;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@ConstraintComposition(CompositionType.AND)
@Min(0)
@Max(0)
@NotNull
public @interface NotZero {
}
