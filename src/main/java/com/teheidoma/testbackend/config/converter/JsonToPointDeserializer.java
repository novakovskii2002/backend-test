package com.teheidoma.testbackend.config.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.teheidoma.testbackend.GeoUtils;
import org.locationtech.jts.geom.Point;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class JsonToPointDeserializer extends JsonDeserializer<Point> {

    @Override
    public Point deserialize(final JsonParser jp, final DeserializationContext ctxt) throws IOException {
        final ArrayNode array = jp.getCodec().readTree(jp);

        final double lat = array.get(0).asDouble();
        final double lon = array.get(1).asDouble();

        return GeoUtils.createPoint(lat, lon);
    }
}
