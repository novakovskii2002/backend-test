package com.teheidoma.testbackend.config.converter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.locationtech.jts.geom.Point;
import org.springframework.boot.jackson.JsonComponent;

import java.io.IOException;

@JsonComponent
public class PointToJsonSerializer extends JsonSerializer<Point> {

    @Override
    public void serialize(final Point value,
                          final JsonGenerator jgen,
                          final SerializerProvider provider) throws IOException {
        jgen.writeArray(new double[]{value.getX(), value.getY()}, 0, 2);
    }

}
