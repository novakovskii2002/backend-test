package com.teheidoma.testbackend.config.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Collections;
import java.util.List;

@Converter
@Slf4j
public class JsonMediaAttributeConverter implements AttributeConverter<List<String>, String> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(final List<String> strings) {
        try {
            return objectMapper.writeValueAsString(strings);
        } catch (JsonProcessingException e) {
            log.warn("Problem with converting json field", e);

            return null;
        }
    }

    @Override
    public List<String> convertToEntityAttribute(final String s) {
        if (s == null || s.isEmpty()) {
            return Collections.emptyList();
        }

        try {
            return objectMapper.readValue(s, List.class);
        } catch (JsonProcessingException e) {
            log.warn("Problem with converting json field", e);

            return Collections.emptyList();
        }
    }
}
