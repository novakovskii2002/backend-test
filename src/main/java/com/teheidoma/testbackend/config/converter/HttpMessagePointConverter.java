package com.teheidoma.testbackend.config.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.teheidoma.testbackend.GeoUtils;
import org.locationtech.jts.geom.Point;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class HttpMessagePointConverter implements Converter<String, Point> {

    private final ObjectMapper jacksonObjectMapper;

    public HttpMessagePointConverter(final ObjectMapper jacksonObjectMapper) {
        this.jacksonObjectMapper = jacksonObjectMapper;
    }

    @Override
    public Point convert(final String source) {
        try {
            final ArrayNode array = (ArrayNode) jacksonObjectMapper.readTree(source);

            final double lat = array.get(0).asDouble();
            final double lon = array.get(1).asDouble();

            return GeoUtils.createPoint(lat, lon);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
