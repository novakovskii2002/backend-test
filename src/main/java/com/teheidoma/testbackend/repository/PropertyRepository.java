package com.teheidoma.testbackend.repository;

import com.teheidoma.testbackend.model.Property;
import org.locationtech.jts.geom.Polygon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PropertyRepository extends JpaRepository<Property, Long> {

    @Query(value = "select t from Property t where intersects(t.point, :area) = true")
    List<Property> findInArea(Polygon area);

}
