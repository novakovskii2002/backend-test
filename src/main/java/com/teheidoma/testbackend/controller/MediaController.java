package com.teheidoma.testbackend.controller;

import com.teheidoma.testbackend.service.MediaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/api/media")
public class MediaController {

    private final MediaService mediaService;

    public MediaController(MediaService mediaService) {
        this.mediaService = mediaService;
    }


    @PostMapping("upload")
    public ResponseEntity<String> upload(@RequestBody final MultipartFile file) {
        try {
            return ResponseEntity.ok(mediaService.saveImage(file.getInputStream(), file.getOriginalFilename()));
        } catch (IOException e) {
            log.error("Error with uploading file", e);

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}
