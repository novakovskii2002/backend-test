package com.teheidoma.testbackend.controller;

import com.teheidoma.testbackend.GeoUtils;
import com.teheidoma.testbackend.dto.GetInAreaRequest;
import com.teheidoma.testbackend.dto.PropertyCreateRequest;
import com.teheidoma.testbackend.model.Property;
import com.teheidoma.testbackend.service.PropertyService;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/property")
public class PropertyController {

    private final PropertyService propertyService;

    public PropertyController(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @PostMapping
    public ResponseEntity<Property> createOrUpdate(@RequestBody final PropertyCreateRequest request) {
        final Point point = GeoUtils.createPoint(request.getPoint().getX(), request.getPoint().getY());

        return ResponseEntity.ok(propertyService.create(request.getId(),
                                                        request.getRoomsCount(),
                                                        request.getSquare(),
                                                        request.getAddress(),
                                                        request.getDescription(),
                                                        request.getMedia(),
                                                        point));
    }

    @GetMapping(value = "/area")
    public List<Property> getInArea(@Valid final GetInAreaRequest request) {
        final Polygon polygon = GeoUtils.createPolygon(
                GeoUtils.createPoint(request.getLeftUpperPoint().getX(), request.getLeftUpperPoint().getY()),
                GeoUtils.createPoint(request.getRightBottomPoint().getX(), request.getRightBottomPoint().getY()));

        return propertyService.getAllInArea(polygon);
    }

    @GetMapping(value = "/id/{id}")
    public ResponseEntity<Property> getById(@PathVariable final long id) {
        return ResponseEntity.of(propertyService.getById(id));
    }

    @GetMapping(value = "/all")
    public List<Property> getAll() {
        return propertyService.getAll();
    }

    @DeleteMapping
    public void delete(@RequestParam final long id) {
        propertyService.delete(id);
    }
}
