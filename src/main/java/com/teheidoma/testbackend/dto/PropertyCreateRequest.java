package com.teheidoma.testbackend.dto;

import com.teheidoma.testbackend.config.NotZero;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.locationtech.jts.geom.Point;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode
public class PropertyCreateRequest {

    private long id;

    @NotBlank
    @NotNull
    private String description;

    @NotZero
    private Double square;

    @NotNull
    private Point point;

    @NotZero
    private Integer roomsCount;

    @NotBlank
    @NotNull
    private String address;

    private List<String> media;
}
