package com.teheidoma.testbackend.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.locationtech.jts.geom.Point;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode
public class GetInAreaRequest {

    @NotNull
    private Point leftUpperPoint;

    @NotNull
    private Point rightBottomPoint;
}
