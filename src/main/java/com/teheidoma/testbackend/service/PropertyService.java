package com.teheidoma.testbackend.service;

import com.teheidoma.testbackend.model.Property;
import com.teheidoma.testbackend.repository.PropertyRepository;
import lombok.extern.slf4j.Slf4j;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PropertyService {

    private final PropertyRepository propertyRepository;

    public PropertyService(PropertyRepository propertyRepository) {
        this.propertyRepository = propertyRepository;
    }

    public Property create(final Long id, final Integer roomsCount, final Double square,
                                          final String address, final String description, final List<String> media,
                                          final Point point) {
        final Property property = Property.builder()
                .id(id)
                .roomsCount(roomsCount)
                .square(square)
                .address(address)
                .description(description)
                .media(media)
                .point(point)
                .build();

        log.debug("creating property {}", property);

        return propertyRepository.save(property);
    }

    public List<Property> getAll() {
        log.debug("fetching all properties");

        return propertyRepository.findAll();
    }

    public List<Property> getAllInArea(final Polygon polygon) {
        log.debug("fetching all in area {}", polygon);

        return propertyRepository.findInArea(polygon);
    }

    public Optional<Property> getById(final Long id) {
        log.debug("fetching by id {}", id);

        return propertyRepository.findById(id);
    }

    public void delete(final Long id) {
        log.debug("delete by id {}", id);

        propertyRepository.deleteById(id);
    }
}
