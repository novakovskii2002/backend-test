package com.teheidoma.testbackend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.DeleteObjectRequest;
import software.amazon.awssdk.services.s3.model.ObjectCannedACL;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.UUID;

@Slf4j
@Service
public class MediaService {

    @Value("${storage.url}")
    private String url;

    @Value("${storage.accessKey}")
    private String accessKey;

    @Value("${storage.accessSecret}")
    private String accessSecret;

    @Value("${storage.bucket}")
    private String bucket;

    private S3Client client;


    @PostConstruct
    public void init() {
        client = S3Client.builder()
                .endpointOverride(URI.create(url))
                .region(Region.AP_NORTHEAST_2)
                .credentialsProvider(() -> AwsBasicCredentials.create(accessKey, accessSecret))
                .build();
    }

    public String saveImage(final InputStream stream, final String originalName) {
        try {
            final String name = generateName(originalName);

            final PutObjectRequest request = PutObjectRequest.builder()
                    .bucket(bucket)
                    .key(name)
                    .contentType("image")
                    .acl(ObjectCannedACL.PUBLIC_READ)
                    .build();

            client.putObject(request, RequestBody.fromInputStream(stream, stream.available()));

            return getResourceUrl(name);
        } catch (IOException e) {
            log.error("can't save image", e);

            return null;
        }
    }

    public void delete(final String name) {
        client.deleteObject(DeleteObjectRequest.builder()
                                    .key(name)
                                    .bucket(bucket)
                                    .build());
    }

    private String generateName(final String originalName) {
        final String[] split = originalName.split("\\.");
        final String ext = split[split.length - 1];
        final String id = UUID.randomUUID().toString();

        return id + "." + ext;
    }

    private String getResourceUrl(final String filename) {
        return String.format("%s/%s/%s", url, bucket, filename);
    }
}
