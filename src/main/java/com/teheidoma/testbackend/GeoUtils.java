package com.teheidoma.testbackend;

import org.locationtech.jts.geom.*;

public final class GeoUtils {

    private static final GeometryFactory GEOMETRY_FACTORY = new GeometryFactory(new PrecisionModel(), 4326);

    private GeoUtils() { }

    public static Point createPoint(final double lat, final double lon) {
        return GEOMETRY_FACTORY.createPoint(new Coordinate(lat, lon));
    }

    public static Polygon createPolygon(final Point topLeft, final Point bottomRight) {
        final Polygon polygon = GEOMETRY_FACTORY.createPolygon(new Coordinate[]{
                topLeft.getCoordinate(),
                new Coordinate(bottomRight.getX(), topLeft.getY()),
                bottomRight.getCoordinate(),
                new Coordinate(topLeft.getX(), bottomRight.getY()),
                topLeft.getCoordinate()
        });

        polygon.normalize();

        return polygon;
    }
}
